<?php


namespace Firewox\Accounts;


class InstitutionPosition
{

  /**
   * @var string
   */
  public $guid;

  /**
   * @var string
   */
  public $name;


  /**
   * @return string
   */
  public function getGuid(): ?string
  {
    return $this->guid;
  }


  /**
   * @return string
   */
  public function getName(): ?string
  {
    return $this->name;
  }


}