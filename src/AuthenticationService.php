<?php

namespace Firewox\Accounts;


use Exception;
use Firewox\Accounts\Exceptions\IntrospectionFailed;
use Firewox\Accounts\Exceptions\NoResponse;
use Firewox\Accounts\Exceptions\NoTokenProvided;
use Firewox\Accounts\Exceptions\ServerErrorResponse;
use Firewox\QEL\Caching\CacheInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Karriere\JsonDecoder\JsonDecoder;

class AuthenticationService
{

  const TOKEN_HINT_ACCESS_TOKEN = 'access_token';
  const TOKEN_HINT_REFRESH_TOKEN = 'refresh_token';

  /**
   * @var string
   */
  private $client_id;

  /**
   * @var string
   */
  private $client_secret;

  /**
   * @var Service
   */
  private $service;

  /**
   * @var CacheInterface|null
   */
  private $cache;

  /**
   * Configure guzzle http client
   * @var array
   */
  private $config;

  /**
   * AuthenticationService constructor.
   * @param string $clientId
   * @param string $clientSecret
   * @param string $discoverUri
   * @param CacheInterface|null $cache
   * @param array $guzzleConfig
   * @throws NoResponse
   * @throws ServerErrorResponse
   * @throws Exceptions\ServiceDiscoveryFailed
   */
  public function __construct(string $clientId, string $clientSecret, string $discoverUri, ?CacheInterface $cache, array $guzzleConfig = [])
  {

    $this->client_id = $clientId;
    $this->client_secret = $clientSecret;
    $this->cache = $cache;

    // Get config for guzzle
    $this->config = array_merge(
      [
        'verify' => false       // Ignore SSL errors by default
      ],
      $guzzleConfig
    );

    // Discover OAuth service
    $this->service = new Service($discoverUri, $this->getHttpClient());

  }


  /**
   * Get user information from token
   * @param string $token
   * @param string $hint
   * @return IntrospectedAccessToken|null
   * @throws IntrospectionFailed
   * @throws NoTokenProvided
   */
  public function introspect(string $token, string $hint = self::TOKEN_HINT_ACCESS_TOKEN): ?IntrospectedAccessToken
  {

    if(!$token) throw new NoTokenProvided();

    // Get access token for introspection service
    $clientAccessToken = $this->getServiceAccessToken();

    try {

      // Load using OAuth client grant
      $client = new Client($this->config);
      $response = $client->post($this->service->getUserInfoEndpoint(), [
        'form_params' => [
          'token' => $token,
          'token_hint' => $hint
        ],
        'headers' => [
          'Authorization' => 'Bearer '.$clientAccessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), IntrospectedAccessToken::class);

      } else if($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (GuzzleException $ex) {

      throw new IntrospectionFailed($ex->getResponse()->getBody()->getContents());

    } catch (Exception $ex) {

      throw new IntrospectionFailed($ex->getMessage());

    }

  }


  /**
   * Get client information
   * @param string $guid
   * @return IntrospectedClient|null
   * @throws IntrospectionFailed
   */
  public function introspectClient(string $guid): ?IntrospectedClient
  {

    // Get access token for introspection service
    $clientAccessToken = $this->getServiceAccessToken();

    try {

      // Load using OAuth client grant
      $client = $this->getHttpClient();
      $response = $client->get($this->service->getClientInfoEndpoint()."/{$guid}", [
        'headers' => [
          'Authorization' => 'Bearer '.$clientAccessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), IntrospectedClient::class);

      } else if($response) {

        throw new Exception('Server responded with code: '.$response->getStatusCode());

      } else {

        throw new Exception('No response received.');

      }

    } catch (GuzzleException $ex) {

      throw new IntrospectionFailed($ex->getResponse()->getBody()->getContents());

    } catch (Exception $ex) {

      throw new IntrospectionFailed($ex->getMessage());

    }

  }


  /**
   * Get service client access token
   * @return string|null
   * @throws IntrospectionFailed
   */
  private function getServiceAccessToken(): ?string {

    // Get access token from redis if exists
    $clientToken = $this->getAccessTokenFromCache();
    if($clientToken) return $clientToken;

    try {

      // Load using OAuth client grant
      $client = $this->getHttpClient();
      $response = $client->post($this->service->getTokenEndpoint(), [
        'form_params' => [
          'grant_type' => 'client_credentials',
          'client_id' => $this->client_id,
          'client_secret' => $this->client_secret,
          'scope' => 'all',
        ]
      ]);

      if ($response->getStatusCode() == 200) {

        // get access token and cache it, then return
        // Decode body to introspection results
        $decoder = new JsonDecoder();

        /* @var AuthResponse $tokenObj */
        $tokenObj = $decoder->decode((string)$response->getBody(), AuthResponse::class);

        // Cache access token
        $this->setAccessTokenToCache($tokenObj->access_token, $tokenObj->expires_in);

        // Return access token
        return $tokenObj->access_token;

      } else if ($response) {

        throw new Exception('Server responded with code: ' . $response->getStatusCode());

      } else {

        throw new Exception('No response received.');

      }

    } catch (GuzzleException $ex) {

      throw new IntrospectionFailed($ex->getResponse()->getBody()->getContents());

    } catch (Exception $ex) {

      throw new IntrospectionFailed($ex->getMessage());

    }

  }


  /**
   * Get service token from in-memory cache
   * @return string|null
   */
  private function getAccessTokenFromCache(): ?string {

    if(!$this->cache) return null;

    $key = 'introspection_service_'.$this->client_id;
    if($this->cache->exists($key)) return $this->cache->get($key);

    // No access token cached
    return null;

  }


  /**
   * Add service token to cache
   * @param string $token
   * @param int $expiresIn
   */
  private function setAccessTokenToCache(string $token, int $expiresIn): void {

    $key = 'introspection_service_'.$this->client_id;

    // Cache token till it expires
    if($this->cache) $this->cache->set($key, $token)->setExpirationTTL($key, $expiresIn);

  }


  /**
   * Get OAuth discovered service
   * @return Service
   */
  public function getService(): Service
  {
    return $this->service;
  }


  /**
   * Get HTTP client for guzzle
   * @return Client
   */
  private function getHttpClient(): Client {
    return new Client($this->config);
  }


  public function getClients(string $token): ?Response
  {

    if(!$token) throw new NoTokenProvided();

    try {

      // Load using OAuth client grant
      $client = $this->getHttpClient();
      $response = $client->get($this->service->getClientsEndpoint(), [
        'headers' => [
          'Authorization' => 'Bearer '.$token
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (GuzzleException $ex) {

      throw new IntrospectionFailed($ex->getResponse()->getBody()->getContents());

    } catch (Exception $ex) {

      throw new IntrospectionFailed($ex->getMessage());

    }

  }


  public function getReports(string $token): ?IntrospectedAccessToken
  {

    if(!$token) throw new NoTokenProvided();

    try {

      // Load using OAuth client grant
      $client = $this->getHttpClient();
      $response = $client->get($this->service->getReportsEndpoint(), [

        'headers' => [
          'Authorization' => 'Bearer '.$token
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (GuzzleException $ex) {

      throw new IntrospectionFailed($ex->getResponse()->getBody()->getContents());

    } catch (Exception $ex) {

      throw new IntrospectionFailed($ex->getMessage());

    }

  }


  public function requestAccessTokenWithAuthCode(string $authCode, string $redirectUri): ?AuthResponse
  {

    if(!$authCode) throw new NoTokenProvided();

    // Load using OAuth client grant
    $client = new Client($this->config);
    $response = $client->post($this->service->getTokenEndpoint(), [
      'form_params' => [
        'grant_type' => 'authorization_code',
        'client_id' => $this->client_id,
        'client_secret' => $this->client_secret,
        'redirect_uri' => $redirectUri,
        'code' => $authCode
      ]
    ]);

    if($response->getStatusCode() == 200) {

      // Decode body to introspection results
      $decoder = new JsonDecoder();
      return $decoder->decode((string) $response->getBody(), AuthResponse::class);

    } else if($response) {

      throw new ServerErrorResponse($response->getStatusCode());

    } else {

      throw new NoResponse();

    }

  }


  public function requestAccessTokenWithRefreshToken(string $refreshToken): ?AuthResponse
  {

    if(!$refreshToken) throw new NoTokenProvided();

    // Load using OAuth client grant
    $client = new Client($this->config);
    $response = $client->post($this->service->getTokenEndpoint(), [
      'form_params' => [
        'grant_type' => 'refresh_token',
        'client_id' => $this->client_id,
        'client_secret' => $this->client_secret,
        'refresh_token' => $refreshToken
      ]
    ]);

    if($response->getStatusCode() == 200) {

      // Decode body to introspection results
      $decoder = new JsonDecoder();
      return $decoder->decode((string) $response->getBody(), AuthResponse::class);

    } else if($response) {

      throw new ServerErrorResponse($response->getStatusCode());

    } else {

      throw new NoResponse();

    }

  }


}