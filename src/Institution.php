<?php


namespace Firewox\Accounts;


use Karriere\JsonDecoder\JsonDecoder;

class Institution
{

  /**
   * @var string
   */
  public $guid;

  /**
   * @var string
   */
  public $reference;

  /**
   * @var string
   */
  public $code;

  /**
   * @var string
   */
  public $name;

  /**
   * @var string
   */
  public $logo;

  /**
   * @var InstitutionPosition|array
   */
  public $position;

  /**
   * @var InstitutionLocation|array
   */
  public $location;


  /**
   * @return string
   */
  public function getGuid(): ?string
  {
    return $this->guid;
  }


  /**
   * @return string
   */
  public function getCode(): ?string
  {
    return $this->code;
  }


  /**
   * @return string
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string
   */
  public function getLogo(): ?string
  {
    return $this->logo;
  }


  /**
   * @return InstitutionPosition
   */
  public function getPosition(): ?InstitutionPosition
  {

    $decode = new JsonDecoder();
    return is_array($this->position) ? $decode->decodeArray($this->position, InstitutionPosition::class) : $this->position;
  }


  /**
   * @return InstitutionLocation
   */
  public function getLocation(): ?InstitutionLocation
  {
    $decode = new JsonDecoder();
    return is_array($this->location) ? $decode->decodeArray($this->location, InstitutionLocation::class) : $this->location;
  }


  /**
   * @return string
   */
  public function getReference(): ?string
  {
    return $this->reference;
  }


}