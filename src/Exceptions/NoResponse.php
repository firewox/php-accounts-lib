<?php

namespace Firewox\Accounts\Exceptions;

class NoResponse extends \Exception
{

    public function __construct(){
        parent::__construct('No response received.');
    }

}