<?php

namespace Firewox\Accounts\Exceptions;

class NotSupported extends \Exception
{

    public function __construct(string $feature){
        parent::__construct($feature.' not supported.');
    }

}