<?php

namespace Firewox\Accounts\Exceptions;

class ServiceDiscoveryFailed extends \Exception
{

    public function __construct(?string $info = null){
        parent::__construct('Service discovery failed to complete. ' . ($info ? $info : 'Nothing specified.'));
    }

}