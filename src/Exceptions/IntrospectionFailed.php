<?php

namespace Firewox\Accounts\Exceptions;

class IntrospectionFailed extends \Exception
{

    public function __construct(?string $info = null){
        parent::__construct('Introspection failed to complete. ' . ($info ? $info : 'Nothing specified.'));
    }

}