<?php


namespace Firewox\Accounts;


class AuthResponse
{

  /**
   * @var string|null
   */
  public $token_type;

  /**
   * @var string|null
   */
  public $access_token;

  /**
   * @var string|null
   */
  public $refresh_token;

  /**
   * @var string|null
   */
  public $expires_in;


  /**
   * @return string|null
   */
  public function getTokenType(): ?string
  {
    return $this->token_type;
  }


  /**
   * @return string|null
   */
  public function getAccessToken(): ?string
  {
    return $this->access_token;
  }


  /**
   * @return string|null
   */
  public function getRefreshToken(): ?string
  {
    return $this->refresh_token;
  }


  /**
   * @return string|null
   */
  public function getExpiresIn(): ?string
  {
    return $this->expires_in;
  }



}