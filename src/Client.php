<?php


namespace Firewox\Accounts;


class Client
{

  /**
   * @var string
   */
  public $guid;

  /**
   * @var string
   */
  public $name;

  /**
   * @var string
   */
  public $description;

  /**
   * @var string
   */
  public $logo;

  /**
   * @var string
   */
  public $homepage;

  /**
   * @var string
   */
  public $type;

  /**
   * @var string
   */
  public $clientguid;

  /**
   * @var bool
   */
  public $allowreporting;

  /**
   * @var bool
   */
  public $allowdashboard;

  /**
   * @var bool
   */
  public $allownotifications;


  /**
   * @return string
   */
  public function getGuid(): ?string
  {
    return $this->guid;
  }


  /**
   * @return string
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }


  /**
   * @return string
   */
  public function getLogo(): ?string
  {
    return $this->logo;
  }


  /**
   * @return string
   */
  public function getHomepage(): ?string
  {
    return $this->homepage;
  }


  /**
   * @return string
   */
  public function getType(): ?string
  {
    return $this->type;
  }


  /**
   * @return string
   */
  public function getClientGuid(): ?string
  {
    return $this->clientguid;
  }


  /**
   * @return bool
   */
  public function isAllowReporting(): ?bool
  {
    return $this->allowreporting;
  }


  /**
   * @return bool
   */
  public function isAllowDashboard(): ?bool
  {
    return $this->allowdashboard;
  }


  /**
   * @return bool
   */
  public function isAllowNotifications(): ?bool
  {
    return $this->allownotifications;
  }



}