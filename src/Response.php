<?php


namespace Firewox\Accounts;


class Response
{

  /**
   * @var int|null
   */
  public $offset;

  /**
   * @var int|null
   */
  public $limit;

  /**
   * @var int|null
   */
  public $count;

  /**
   * @var array|null
   */
  public $data;


  /**
   * @return int|null
   */
  public function getOffset(): ?int
  {
    return $this->offset;
  }


  /**
   * @return int|null
   */
  public function getLimit(): ?int
  {
    return $this->limit;
  }


  /**
   * @return int|null
   */
  public function getCount(): ?int
  {
    return $this->count;
  }


  /**
   * @return array|null
   */
  public function getData(): ?array
  {
    return $this->data;
  }


}