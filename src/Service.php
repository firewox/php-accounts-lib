<?php


namespace Firewox\Accounts;


use Exception;
use Firewox\Accounts\Exceptions\NoResponse;
use Firewox\Accounts\Exceptions\NotSupported;
use Firewox\Accounts\Exceptions\ServerErrorResponse;
use Firewox\Accounts\Exceptions\ServiceDiscoveryFailed;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Karriere\JsonDecoder\JsonDecoder;

class Service
{

  /**
   * @var string
   */
  public $issuer;

  /**
   * @var string
   */
  public $authorization_endpoint;

  /**
   * @var string
   */
  public $token_endpoint;

  /**
   * @var string
   */
  public $userinfo_endpoint;

  /**
   * @var string
   */
  public $clientinfo_endpoint;

  /**
   * @var string
   */
  public $revoke_endpoint;

  /**
   * @var string[]
   */
  public $scopes_supported;

  /**
   * @var string[]
   */
  public $response_types_supported;

  /**
   * @var string[]
   */
  public $response_modes_supported;

  /**
   * @var string
   */
  public $discovery_endpoint;

  /**
   * @var string
   */
  public $clients_endpoint;

  /**
   * @var string
   */
  public $reports_endpoint;

  /**
   * @var string
   */
  public $connectuser_endpoint;

  /**
   * @var string
   */
  public $license_endpoint;

  /**
   * @var string
   */
  public $invoicing_endpoint;

  /**
   * @var string
   */
  public $modules_endpoint;

  /**
   * @var string
   */
  public $modulespurchase_endpoint;


  /**
   * Service constructor.
   * @param string $discoveryEndpoint
   * @param Client $client
   * @throws NoResponse
   * @throws ServerErrorResponse
   * @throws ServiceDiscoveryFailed
   */
  public function __construct(?string $discoveryEndpoint = null, ?Client $client = null)
  {
    if($discoveryEndpoint) $this->discovery_endpoint = $discoveryEndpoint;
    if($discoveryEndpoint && $client) $this->discover($client);
  }


  /**
   * Discover OAuth service
   * @param Client $client
   * @return bool
   * @throws NoResponse
   * @throws ServerErrorResponse
   * @throws ServiceDiscoveryFailed
   */
  private function discover(Client $client): bool {

    // Get response checking error
    try {

      $response = $client->get($this->discovery_endpoint);

    } catch (GuzzleException $ex) {

      throw new ServiceDiscoveryFailed($ex->getResponse()->getBody()->getContents());

    } catch (Exception $ex) {

      throw new ServiceDiscoveryFailed($ex->getMessage());

    }

    if($response->getStatusCode() == 200) {

      // Decode body to introspection results
      $decoder = new JsonDecoder();
      $this->copyFromSelf(
        $decoder->decode((string) $response->getBody(), Service::class)
      );

      return true;

    } else if($response) {

      throw new ServerErrorResponse($response->getStatusCode());

    } else {

      throw new NoResponse();

    }

  }


  /**
   * Copy object
   * @param Service|null $self
   */
  private function copyFromSelf(?Service $self)
  {

    if(!$self) return;

    // Set properties
    $this->issuer = $self->issuer;
    $this->authorization_endpoint = $self->authorization_endpoint;
    $this->token_endpoint = $self->token_endpoint;
    $this->userinfo_endpoint = $self->userinfo_endpoint;
    $this->clientinfo_endpoint = $self->clientinfo_endpoint;
    $this->revoke_endpoint = $self->revoke_endpoint;
    $this->scopes_supported = $self->scopes_supported;
    $this->response_types_supported = $self->response_types_supported;
    $this->response_modes_supported = $self->response_modes_supported;
    $this->reports_endpoint = $self->reports_endpoint;
    $this->clients_endpoint = $self->clients_endpoint;
    $this->invoicing_endpoint = $self->invoicing_endpoint;
    $this->license_endpoint = $self->license_endpoint;
    $this->modules_endpoint = $self->modules_endpoint;
    $this->connectuser_endpoint = $self->connectuser_endpoint;
    $this->modulespurchase_endpoint = $self->modulespurchase_endpoint;

  }

  /**
   * @return string
   * @throws NotSupported
   */
  public function getIssuer(): string
  {
    if(!$this->issuer) throw new NotSupported('Issuer');
    return $this->issuer;
  }

  /**
   * @return string
   * @throws NotSupported
   */
  public function getAuthorizationEndpoint(): string
  {
    if(!$this->authorization_endpoint) throw new NotSupported('Authorization endpoint');
    return $this->authorization_endpoint;
  }

  /**
   * @return string
   * @throws NotSupported
   */
  public function getTokenEndpoint(): string
  {
    if(!$this->token_endpoint) throw new NotSupported('Token endpoint');
    return $this->token_endpoint;
  }

  /**
   * @return string
   * @throws NotSupported
   */
  public function getUserInfoEndpoint(): string
  {
    if(!$this->userinfo_endpoint) throw new NotSupported('User information endpoint');
    return $this->userinfo_endpoint;
  }

  /**
   * @return string
   * @throws NotSupported
   */
  public function getClientInfoEndpoint(): string
  {
    if(!$this->clientinfo_endpoint) throw new NotSupported('Client information endpoint');
    return $this->clientinfo_endpoint;
  }

  /**
   * @return string
   * @throws NotSupported
   */
  public function getRevokeEndpoint(): string
  {
    if(!$this->revoke_endpoint) throw new NotSupported('Revokation endpoint');
    return $this->revoke_endpoint;
  }

  /**
   * @return string[]|null
   * @throws NotSupported
   */
  public function getScopesSupported(): ?array
  {
    if(!$this->scopes_supported) throw new NotSupported('Scopes');
    return $this->scopes_supported;
  }

  /**
   * @return string[]|null
   * @throws NotSupported
   */
  public function getResponseTypesSupported(): ?array
  {
    if(!$this->response_types_supported) throw new NotSupported('Response types');
    return $this->response_types_supported;
  }

  /**
   * @return string[]|null
   * @throws NotSupported
   */
  public function getResponseModesSupported(): ?array
  {
    if(!$this->response_modes_supported) throw new NotSupported('Response modes');
    return $this->response_modes_supported;
  }

  /**
   * @return string|null
   */
  public function getDiscoveryEndpoint(): ?string
  {
    return $this->discovery_endpoint;
  }

  /**
   * @return string|null
   */
  public function getClientsEndpoint(): ?string
  {
    return $this->clients_endpoint;
  }

  /**
   * @return string|null
   */
  public function getReportsEndpoint(): ?string
  {
    return $this->reports_endpoint;
  }

  /**
   * @return string|null
   */
  public function getConnectUserEndpoint(): ?string
  {
    return $this->connectuser_endpoint;
  }

  /**
   * @return string|null
   */
  public function getLicenseEndpoint(): ?string
  {
    return $this->license_endpoint;
  }

  /**
   * @return string|null
   */
  public function getInvoicingEndpoint(): ?string
  {
    return $this->invoicing_endpoint;
  }

  /**
   * @return string|null
   */
  public function getModulesEndpoint(): ?string
  {
    return $this->modules_endpoint;
  }

  /**
   * @return string|null
   */
  public function getModulesPurchaseEndpoint(): ?string
  {
    return $this->modulespurchase_endpoint;
  }


}