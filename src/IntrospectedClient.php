<?php


namespace Firewox\Accounts;


class IntrospectedClient
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $description;

  /**
   * @var string|null
   */
  public $logo;

  /**
   * @var string|null
   */
  public $type;

  /**
   * @var bool|null
   */
  public $hassecret;

  /**
   * @var bool|null
   */
  public $haskey;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }


  /**
   * @return string|null
   */
  public function getLogo(): ?string
  {
    return $this->logo;
  }


  /**
   * @return string|null
   */
  public function getType(): ?string
  {
    return $this->type;
  }


  /**
   * @return bool|null
   */
  public function hasSecret(): ?bool
  {
    return $this->hassecret;
  }


  /**
   * @return bool|null
   */
  public function haskey(): ?bool
  {
    return $this->haskey;
  }


}