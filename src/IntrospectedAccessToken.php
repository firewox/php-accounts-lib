<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/20/2019
 * Time: 12:13 AM
 */

namespace Firewox\Accounts;


use Karriere\JsonDecoder\JsonDecoder;

class IntrospectedAccessToken
{

  /**
   * @var bool
   */
  public $active;

  /**
   * @var string
   */
  public $token;

  /**
   * @var string
   */
  public $client_id;

  /**
   * @var string
   */
  public $username;

  /**
   * @var string
   */
  public $firstname;

  /**
   * @var string
   */
  public $lastname;

  /**
   * @var string
   */
  public $mobile;

  /**
   * @var string
   */
  public $email;

  /**
   * @var string
   */
  public $avatar;

  /**
   * @var string
   */
  public $expireson;

  /**
   * @var bool
   */
  public $issuper;

  /**
   * @var bool
   */
  public $isdeveloper;

  /**
   * @var bool
   */
  public $isadmin;

  /**
   * @var bool
   */
  public $isappdeveloper;

  /**
   * @var bool
   */
  public $isappsuper;

  /**
   * @var Institution[]
   */
  public $institutions;


  /**
   * @return bool
   */
  public function getActive(): ?bool
  {
    return $this->active;
  }


  /**
   * @return string|null
   */
  public function getToken(): ?string
  {
    return $this->token;
  }


  /**
   * @return string|null
   */
  public function getClientId(): ?string
  {
    return $this->client_id;
  }


  /**
   * @return string|null
   */
  public function getUsername(): ?string
  {
    return $this->username;
  }


  /**
   * @return string|null
   */
  public function getFirstname(): ?string
  {
    return $this->firstname;
  }


  /**
   * @return string|null
   */
  public function getLastname(): ?string
  {
    return $this->lastname;
  }


  /**
   * @return string|null
   */
  public function getMobile(): ?string
  {
    return $this->mobile;
  }


  /**
   * @return string|null
   */
  public function getEmail(): ?string
  {
    return $this->email;
  }


  /**
   * @return string|null
   */
  public function getAvatar(): ?string
  {
    return $this->avatar;
  }


  /**
   * @return string|null
   */
  public function getExpireson(): ?string
  {
    return $this->expireson;
  }


  /**
   * @return bool
   */
  public function isSuper(): ?bool
  {
    return $this->issuper;
  }


  /**
   * @return bool
   */
  public function isDeveloper(): ?bool
  {
    return $this->isdeveloper;
  }


  /**
   * @return bool
   */
  public function isAdmin(): ?bool
  {
    return $this->isadmin;
  }


  /**
   * @return bool
   */
  public function isAppDeveloper(): ?bool
  {
    return $this->isappdeveloper;
  }


  /**
   * @return bool
   */
  public function isAppSuper(): ?bool
  {
    return $this->isappsuper;
  }


  /**
   * @return Institution[]
   */
  public function getInstitutions(): array
  {
    $decoder = new JsonDecoder();
    return $decoder->decodeMultiple(json_encode($this->institutions), Institution::class);
  }



}