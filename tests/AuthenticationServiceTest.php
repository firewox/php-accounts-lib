<?php


use Firewox\Accounts\AuthenticationService;
use Firewox\Accounts\IntrospectedAccessToken;
use Firewox\Accounts\IntrospectedClient;
use PHPUnit\Framework\TestCase;

class AuthenticationServiceTest extends TestCase
{


  public function testIntrospectAccessToken() {

    // Create auth / introspection / revocation service
    $service = new AuthenticationService(
      'CLIENT ID',
      'CLIENT SECRET',
      'DISCOVER URI',
      null);

    // Introspect client GUID
    /* @var IntrospectedAccessToken $ic */
    $ic = $service->introspect('ACCESS OR REFRESH TOKEN');

    $this->assertSame('test_user_name', $ic->username);

  }


  public function testIntrospectClient() {

    // Create auth / introspection / revocation service
    $service = new AuthenticationService(
      'CLIENT ID',
      'CLIENT SECRET',
      'DISCOVER URI',
      null);

    // Introspect client GUID
    /* @var IntrospectedClient $ic */
    $ic = $service->introspectClient('CLIENT GUID');

    $this->assertSame('Web', $ic->type);

  }


  public function testAuthCode() {

    // Create auth / introspection / revocation service
    $service = new AuthenticationService(
      'CLIENT ID',
      'CLIENT SECRET',
      'DISCOVER URI',
      null);

    $code = "AUTH CODE";
    $uri = "REDIRECT URI";

    // Introspect client GUID
    /* @var IntrospectedClient $ic */
    $ar = $service->requestAccessToken($code, $uri);

    $this->assertSame('Bearer', $ar->token_type);

  }

}
