# PHP Firewox Accounts Utility Library

Library for token introspection and revokation on the Firewox authentication cloud.

## Getting Started

Use composer package manager to install this library.

```bash
composer require firewox/accounts
```